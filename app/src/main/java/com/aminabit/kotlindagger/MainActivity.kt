package com.aminabit.kotlindagger

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.aminabit.kotlindagger.di.ComputerModule
import com.aminabit.kotlindagger.di.DaggerComputerComponent
import com.aminabit.kotlindagger.model.Computer
import javax.inject.Inject
import javax.inject.Named

class MainActivity : AppCompatActivity() {

 /*
 Age bekham az Named estefade konam:
 @field:[Inject Named("chrome")]
  lateinit var computer: Computer*/

  /*
  * bedune named:
  @field:[Inject]
  lateinit var computer: Computer
  */

  @Inject
  lateinit var computer: Computer

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    DaggerComputerComponent.builder().computerModule(ComputerModule("intel",7,64,"SSD")).build().inject(this)

    Toast.makeText(this,computer.model,Toast.LENGTH_LONG).show()
  }
}
