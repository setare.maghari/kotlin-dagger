package com.aminabit.kotlindagger.di

import com.aminabit.kotlindagger.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Setare on 11/11/2017.
 */
@Singleton
@Component(modules = arrayOf(ComputerModule::class))
interface ComputerComponent {
  fun inject(activity: MainActivity)
}