package com.aminabit.kotlindagger.di

import com.aminabit.kotlindagger.model.Computer
import com.aminabit.kotlindagger.model.Cpu
import com.aminabit.kotlindagger.model.Ram
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Setare on 11/11/2017.
 */
@Module
class ComputerModule(val cpuName: String, val cpuCore: Int, val ramCapacity: Int,
    var ramModel: String)

{
  @Provides
  @Singleton
  fun provideCpu(): Cpu{
    return Cpu(cpuName,cpuCore)
  }


  @Provides
  @Singleton
  fun provideRam(): Ram{
    return Ram(ramModel,ramCapacity)
  }

  @Provides
  @Singleton
  fun provideComputer(ram: Ram,  cpu: Cpu): Computer{
    return Computer("lenovo",ram,cpu)
  }


  @Provides
  @Singleton
  @Named("chrome")
  fun provideChromBook(ram: Ram,  cpu: Cpu): Computer{
    return Computer("chromeBook",ram,cpu)
  }
}