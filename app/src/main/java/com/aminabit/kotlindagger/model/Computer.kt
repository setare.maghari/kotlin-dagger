package com.aminabit.kotlindagger.model

import javax.inject.Inject

/**
 * Created by Setare on 11/11/2017.
 */
class Computer @Inject constructor(val model: String, val ram: Ram, val cpu: Cpu){

}