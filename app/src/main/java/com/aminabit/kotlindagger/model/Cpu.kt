package com.aminabit.kotlindagger.model

/**
 * Created by Setare on 11/11/2017.
 */
class Cpu(val name: String, val core: Int)